package Bussines;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Logic.Client;
import Presentation.MainWindow;

public class AddClient extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
    private JTextField input1 = null;
    private JTextField input2 = null;
    
    private String fName = null;
    private String lName = null;
    
    private JButton addBtn = null;
    private JButton backBtn = null;
	
    private Client client = null;
    
	public static void newAddClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddClient frame = new AddClient();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public AddClient() {
		
		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("New client");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		JPanel contentPane3 = new JPanel(new FlowLayout());
		JPanel contentPane4 = new JPanel(new FlowLayout());
		
		
		input1 = new JTextField(30);
		input2 = new JTextField(30);
		
		JTextField txt1 = new JTextField();
		txt1.setText("First Name:");
		txt1.setEditable(false);
		
		JTextField txt2 = new JTextField();
		txt2.setText("Last Name:");
		txt2.setEditable(false);
		
		contentPane3.add(txt1);
		contentPane3.add(input1);
		contentPane4.add(txt2);
		contentPane4.add(input2);
		
		client = new Client();
		
		//add new customer button
		addBtn = new JButton("Add customer");
		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				fName = input1.getText();
				lName = input2.getText();
				input1.setText("");
				input2.setText("");
				if(client.addClient(fName, lName) == true) JOptionPane.showMessageDialog(null, "Success");  	
				else JOptionPane.showMessageDialog(null, "Fail");  
			}
		});
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
		
		contentPane.add(contentPane3);
		contentPane.add(contentPane4);
		contentPane.add(addBtn);
		contentPane.add(backBtn);
		
		contentPane2.add(contentPane);
		setContentPane(contentPane2);
	}
}
