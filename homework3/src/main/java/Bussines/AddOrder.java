package Bussines;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Logic.Order;
import Presentation.MainWindow;

public class AddOrder extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
    private JTextField input1 = null;
    private JTextField input2 = null;
    private JTextField input3 = null;
    private JTextField input4 = null;
    
    private String fName = null;
    private String lName = null;
    private String pName = null;
    private int quantity = -1;
    
    private JButton addBtn = null;
    private JButton addBtn2 = null;
    private JButton backBtn = null;
    
    private Order order = null;
    

	public static void newAddClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddOrder frame = new AddOrder();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    
	
    public AddOrder() {

		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("New order");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		JPanel contentPane3 = new JPanel(new FlowLayout());
		JPanel contentPane4 = new JPanel(new FlowLayout());
		JPanel contentPane5 = new JPanel(new FlowLayout());
		JPanel contentPane6 = new JPanel(new FlowLayout());
		
		input1 = new JTextField(30);
		input2 = new JTextField(30);
		input3 = new JTextField(30);
		input4 = new JTextField(30);
		
		JTextField txt1 = new JTextField();
		txt1.setText("First Name:");
		txt1.setEditable(false);
		
		JTextField txt2 = new JTextField();
		txt2.setText("Last Name:");
		txt2.setEditable(false);
		
		JTextField txt3 = new JTextField();
		txt3.setText("Product Name:");
		txt3.setEditable(false);
		
		JTextField txt4 = new JTextField();
		txt4.setText("Quantity:");
		txt4.setEditable(false);
		
		contentPane3.add(txt1);
		contentPane3.add(input1);
		contentPane4.add(txt2);
		contentPane4.add(input2);
		contentPane5.add(txt3);
		contentPane5.add(input3);
		contentPane6.add(txt4);
		contentPane6.add(input4);
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
		
		order = new Order();
		
		
		//create new order
		addBtn = new JButton("Create order");
		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				getFields();
				order.createOrder(fName, lName);
			}
		});
		
		//add product to order
		addBtn2 = new JButton("Add product to order");
		addBtn2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				getFields();
				order.updateOrder(fName, lName, pName, quantity);
			}
		});
		
		contentPane.add(contentPane3);
		contentPane.add(contentPane4);
		contentPane.add(contentPane5);
		contentPane.add(contentPane6);
		contentPane.add(addBtn);
		contentPane.add(addBtn2);
		contentPane.add(backBtn);
		
		contentPane2.add(contentPane);
		setContentPane(contentPane2);
		
    }
    
    private void getFields() {
    	fName = input1.getText();
    	lName = input2.getText();
    	pName = input3.getText();
    	quantity = Integer.parseInt(input4.getText());
    }
}
