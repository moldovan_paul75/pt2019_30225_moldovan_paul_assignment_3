package Bussines;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Logic.Product;
import Presentation.MainWindow;

public class AddProduct extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
    private JTextField input1 = null;
    private JTextField input2 = null;
    private JTextField input3 = null;
    
    private JButton addBtn = null;
    private JButton backBtn = null;
    
    private Product product = null;
    
    private String pName = null;
    private double price;
    private int stock;
    
    
	public static void newAddProductWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProduct frame = new AddProduct();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
    
    public AddProduct() {
    	
		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("New product");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		JPanel contentPane3 = new JPanel(new FlowLayout());
		JPanel contentPane4 = new JPanel(new FlowLayout());
		JPanel contentPane5 = new JPanel(new FlowLayout());
		
		input1 = new JTextField(30);
		input2 = new JTextField(30);
		input3 = new JTextField(30);
		
		JTextField txt1 = new JTextField();
		txt1.setText("Product Name:");
		txt1.setEditable(false);
		
		JTextField txt2 = new JTextField();
		txt2.setText("Price:");
		txt2.setEditable(false);
		
		JTextField txt3 = new JTextField();
		txt3.setText("Stock:");
		txt3.setEditable(false);
		
		contentPane3.add(txt1);
		contentPane3.add(input1);
		contentPane4.add(txt2);
		contentPane4.add(input2);
		contentPane5.add(txt3);
		contentPane5.add(input3);
		
		product = new Product();
		
		//add new customer button
		addBtn = new JButton("Add product");
		addBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				pName = input1.getText();
				price = Double.parseDouble(input2.getText());
				stock = Integer.parseInt(input3.getText());
				input1.setText("");
				input2.setText("");
				input3.setText("");
				product.addProduct(pName, price, stock);
				JOptionPane.showMessageDialog(null, "Success");  	
			}
		});
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
    	
		contentPane.add(contentPane3);
		contentPane.add(contentPane4);
		contentPane.add(contentPane5);
		contentPane.add(addBtn);
		contentPane.add(backBtn);
		
		contentPane2.add(contentPane);
		setContentPane(contentPane2);
    }
    
}
