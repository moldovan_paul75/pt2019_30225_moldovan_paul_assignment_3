package Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import db.DBConnection;

public class Client {
	
	DBConnection db = new DBConnection();
	final Connection connection = db.getConnection();
	
	
	public ResultSet getClients() {
		
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		//List<String> result = new ArrayList<String>();
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM Customer");
			resultSet = preparedStatement.executeQuery();
			
			
			/*while(resultSet.next()) {
				result.add(resultSet.getString("CustomerId") + " " +
				 resultSet.getString("firstName") + " " +
				  resultSet.getString("lastName"));
			}*/
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	
	public boolean addClient(String fName, String lName) {
		
		PreparedStatement preparedStatement;
		int i = -1;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO Customer(firstName, lastName) VALUES (?, ?)");
			preparedStatement.setString(1, fName);
			preparedStatement.setString(2, lName);
			i = preparedStatement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		if(i > 0 ) return true;
		else return false;
	}
	
	public boolean deleteClient(int CustomerId) {
		
		PreparedStatement preparedStatement;
		int i = -1;
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM Customer WHERE Customer.CustomerId = ? LIMIT 1");
			preparedStatement.setInt(1, CustomerId);
			i = preparedStatement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		if(i > 0 ) return true;
		else return false;
	}
	
	
	public boolean updateClient(int id, String fName, String lName) {

		PreparedStatement preparedStatement;
		int i = -1;
		try {
			preparedStatement = connection.prepareStatement("UPDATE Customer SET Customer.firstName = ?, Customer.lastName = ? WHERE Customer.CustomerId = ?");
			preparedStatement.setString(1, fName);
			preparedStatement.setString(2, lName);
			preparedStatement.setInt(3,  id);
			i = preparedStatement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		if(i > 0 ) return true;
		else return false;
	}
}
