package Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.DBConnection;

public class Order {
	DBConnection db = new DBConnection();
	final Connection connection = db.getConnection();
	
	public ResultSet getOrders() {
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		//List<String> result = new ArrayList<String>();
		
		try {
			preparedStatement = connection.prepareCall("SELECT * FROM warehouse.view_orders");
			resultSet = preparedStatement.executeQuery();
			/*while(resultSet.next()) {
				result.add(resultSet.getString("ID").toString() + " "
						  + resultSet.getString("PRODUCT NAME").toString() + " "
						  +resultSet.getString("PRICE").toString() + " "
						  +resultSet.getString("STOCK").toString());
			}*/
			//System.out.println(result);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	
	public void updateOrder(String fName, String lName, String pName, int quantity) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareCall("{call ADD_PRODUCT_TO_ORDER(?, ?, ?, ?)}");
			preparedStatement.setString(1, fName);
			preparedStatement.setString(2, lName);
			preparedStatement.setString(3, pName);
			preparedStatement.setInt(4, quantity);
			preparedStatement.executeQuery();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void createOrder(String fName, String lName) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareCall("{call ADD_ORDER(?, ?)}");
			preparedStatement.setString(1, fName);
			preparedStatement.setString(2, lName);
			preparedStatement.executeQuery();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteOrder(int id) {
		
		PreparedStatement preparedStatement;
		
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM Orders WHERE Orders.OrderId = ?");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
