package Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.DBConnection;

public class Product {
	DBConnection db = new DBConnection();
	final Connection connection = db.getConnection();
	
	public ResultSet getProducts(){
		PreparedStatement preparedStatement;
		ResultSet resultSet = null;
		//List<String> result = new ArrayList<String>();
		
		try {
			preparedStatement = connection.prepareCall("SELECT * FROM warehouse.view_products");
			resultSet = preparedStatement.executeQuery();
			/*while(resultSet.next()) {
				result.add(resultSet.getString("ID").toString() + " "
						  + resultSet.getString("PRODUCT NAME").toString() + " "
						  +resultSet.getString("PRICE").toString() + " "
						  +resultSet.getString("STOCK").toString());
			}*/
			//System.out.println(result);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	
	public void addProduct(String pName, double price, int stock) {
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareCall("{call ADD_PRODUCT(?, ?, ?)}");
			preparedStatement.setString(1, pName);
			preparedStatement.setDouble(2, price);
			preparedStatement.setInt(3, stock);
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateStock(int id, int stock) {
		PreparedStatement preparedStatement;
		
		try {
			preparedStatement = connection.prepareCall("UPDATE ProductQ SET ProductQ.Stock = ? WHERE ProductQ.ProductId = ?");
			preparedStatement.setInt(1, stock);
			preparedStatement.setInt(2, id);
			preparedStatement.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updatePrice(int id, double price) {
		PreparedStatement preparedStatement;
		
		try {
			preparedStatement = connection.prepareCall("UPDATE Product SET Product.ListPrice = ? WHERE Product.ProductId = ?");
			preparedStatement.setDouble(1, price);
			preparedStatement.setInt(2, id);
			preparedStatement.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteProduct(int id) {
		
		PreparedStatement preparedStatement;
		
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM Product WHERE Product.ProductId = ? LIMIT 1");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

}