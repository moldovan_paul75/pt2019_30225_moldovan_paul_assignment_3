package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import Logic.Client;


public class ClientW extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private Client client = null;
	private JTable table = null;
	DefaultTableModel model = null;
	
	private JButton backBtn = null;
	private JButton saveBtn = null;
	private JButton deleteBtn = null;
	
	private int id = -1;
	private String fName = null;
	private String lName = null;
	
	private int id1 = -1;
	private String fName1 = null;
	private String lName1 = null;
	
	
	public static void newClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientW frame = new ClientW();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ClientW() {
		
		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("Client");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(540, 220);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		//clients table
		table = new JTable();
		client = new Client();
		model = (DefaultTableModel) table.getModel();
		try {
			ResultSet rst = client.getClients();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			int colCount = rsmd.getColumnCount();
			
			for (int i=1; i <= colCount; i++){	
				model.addColumn(rsmd.getColumnName(i).toString());
			}
			while (rst.next()){	
				model.addRow(new Object[] {rst.getString("CustomerId").toString(), 
						rst.getString("firstName").toString(), 
						rst.getString("lastName").toString()});
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
		
		
		//save button
		saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(id != -1) {
					if(client.updateClient(id, fName, lName) == true) JOptionPane.showMessageDialog(null, "Success");  	
					else JOptionPane.showMessageDialog(null, "Fail"); 
					//System.out.println(id1+fName1+lName1);
					id = -1;
					fName = null;
					lName = null;
				}
			}	
		});
		
		
		//delete button
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(id != -1) {
					if(client.deleteClient(id) == true)  JOptionPane.showMessageDialog(null, "Success");  	
					else JOptionPane.showMessageDialog(null, "Fail"); 
					id=-1;
					fName = null;
					lName = null;
					reloadTable();
				}
			}
		});
		
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});

		
		table.getModel().addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {	
				//if(id == -1) getUpdatedRow(e);
			}
		});

		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane2.add(backBtn);
		contentPane2.add(saveBtn);
		contentPane2.add(deleteBtn);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
	
	
	private void reloadTable() {
		try {
			ResultSet rst = client.getClients();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			model.setRowCount(0);
			int colCount = rsmd.getColumnCount();
			
			while (rst.next()){	
				model.addRow(new Object[] {rst.getString("CustomerId").toString(), rst.getString("firstName").toString(), rst.getString("lastName").toString()});
			}
			model.fireTableDataChanged();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void getTableRow(MouseEvent e) {
		int row = table.getSelectedRow();
		id = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
  		fName = table.getModel().getValueAt(row, 1).toString();
  		lName = table.getModel().getValueAt(row, 2).toString();
	}
	
	private void getUpdatedRow(TableModelEvent e) {
		int row = table.getSelectedRow();
		id1 = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
  		fName1 = table.getModel().getValueAt(row, 1).toString();
  		lName1 = table.getModel().getValueAt(row, 2).toString();
	}
}
