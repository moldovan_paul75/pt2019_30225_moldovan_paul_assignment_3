package Presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import Bussines.AddClient;
import Bussines.AddOrder;
import Bussines.AddProduct;
import Logic.Client;

public class MainWindow extends JFrame{
	
	private JPanel contentPane = null;
	private JButton viewCustomers = null;
	private JButton viewProducts = null;
	private JButton viewOrders = null;
	private JButton addCustomer = null;
	private JButton addProduct = null;
	private JButton addOrder = null;
	
	public static void newMainWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public MainWindow() {
		
		
		setBounds(100, 100, 405, 294);
    	setTitle("Main");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(540, 220);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(2, 3, 5, 5));
		
		
		viewCustomers = new JButton("View customers");
		viewCustomers.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				ClientW.newClientWindow();
			}
		});
		contentPane.add(viewCustomers);
		
		viewProducts = new JButton("View products");
		viewProducts.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				ProductW.newProductWindow();
			}
		});
		contentPane.add(viewProducts);
		
		viewOrders = new JButton("View orders");
		viewOrders.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				OrderW.newOrderWindow();
			}
		});
		contentPane.add(viewOrders);
		
		addCustomer = new JButton("New customer");
		addCustomer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AddClient.newAddClientWindow();
			}
			
		});
		contentPane.add(addCustomer);
		
		addProduct = new JButton("New product");
		addProduct.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AddProduct.newAddProductWindow();
			}
		});
		contentPane.add(addProduct);
		
		addOrder = new JButton("New order");
		addOrder.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				AddOrder.newAddClientWindow();
			}
		});
		contentPane.add(addOrder);
		
		setContentPane(contentPane);
	}
	
	
}