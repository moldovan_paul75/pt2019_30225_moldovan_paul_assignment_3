package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import Logic.Order;

public class OrderW extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private Order order = null;
	private JTable table = null;
	DefaultTableModel model = null;
	
	private JButton backBtn = null;
	private JButton deleteBtn = null;
	private JButton billBtn = null;
	
	private int id = -1;
	private String fName = null;
	private String lName = null;
	private String pName = null;
	private int quantity;
	private double total;
	private String datetime = null;
	
	public static void newOrderWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderW frame = new OrderW();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public OrderW() {
		
		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("Order");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(640, 280);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		//products table
		table = new JTable();
		order = new Order();
		model = (DefaultTableModel) table.getModel();
		try {
			ResultSet rst = order.getOrders();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			int colCount = rsmd.getColumnCount();
			
			for (int i=1; i <= colCount; i++){	
				model.addColumn(rsmd.getColumnName(i).toString());
			}
			while (rst.next()){	
				model.addRow(new Object[] {rst.getString("ID").toString(), 
						rst.getString("DATE").toString(), 
						rst.getString("FIRST NAME").toString(),
						rst.getString("LAST NAME").toString(),
						rst.getString("TOTAL").toString(),
						rst.getString("PRODUCT NAME").toString(),
						rst.getString("PRODUCT ID").toString(),
						rst.getString("QUANTITY").toString()});
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
		
		//bill button
		billBtn = new JButton("Bill");
		billBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PrintWriter writer;
				try {
					if(id!=-1) {
						writer = new PrintWriter("bill.txt", "UTF-8");
						writer.println("Order id: "+ id+"\n"+ "Customer: " + fName + " "+ lName + "\n" + "Product: " + pName + "\n" + "Quantity: " + quantity + "\n\n" + "Total: "+ total);
						writer.close();
					}
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		//delete button
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(id != -1) {
					order.deleteOrder(id);
					id = -1;
					reloadTable();
				}
			}
		});
		
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});

		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane2.add(backBtn);
		contentPane2.add(deleteBtn);
		contentPane2.add(billBtn);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
	
	private void reloadTable() {
		try {
			ResultSet rst = order.getOrders();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			model.setRowCount(0);
			int colCount = rsmd.getColumnCount();

			while (rst.next()){	
				model.addRow(new Object[] {rst.getString("ID").toString(), 
						rst.getString("DATE").toString(), 
						rst.getString("FIRST NAME").toString(),
						rst.getString("LAST NAME").toString(),
						rst.getString("TOTAL").toString(),
						rst.getString("PRODUCT NAME").toString(),
						rst.getString("PRODUCT ID").toString(),
						rst.getString("QUANTITY").toString()});
			}
			
			model.fireTableDataChanged();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private void getTableRow(MouseEvent e) {
		int row = table.getSelectedRow();
		id = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
		datetime = table.getModel().getValueAt(row, 1).toString();
		fName = table.getModel().getValueAt(row, 2).toString();
		lName = table.getModel().getValueAt(row, 3).toString();
		total = Double.parseDouble(table.getModel().getValueAt(row, 4).toString());
		pName = table.getModel().getValueAt(row, 5).toString();
		quantity = Integer.parseInt(table.getModel().getValueAt(row, 7).toString());
	}
	
}
