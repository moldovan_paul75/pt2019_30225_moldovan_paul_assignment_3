package Presentation;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import Logic.Client;
import Logic.Product;

public class ProductW extends JFrame{
	
	private JPanel contentPane = null;
	private JPanel contentPane2 = null;
	
	private Product product = null;
	private JTable table = null;
	DefaultTableModel model = null;
	
	private JButton backBtn = null;
	private JButton saveBtn = null;
	private JButton deleteBtn = null;
	
	private int id = -1;
	
	private int id1 = -1;
	private String pName = null;
	private double price = -1.f;
	private int stock = -1;
	
	
	public static void newProductWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductW frame = new ProductW();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ProductW() {
	
		//window setup
		setBounds(100, 100, 405, 294);
    	setTitle("Product");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(540, 220);
    	setResizable(false);
    	setLocation(780, 220);
    	
    	//content panel
    	contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane2 = new JPanel();
		contentPane2.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane2.setLayout(new FlowLayout());
		
		//products table
		table = new JTable();
		product = new Product();
		model = (DefaultTableModel) table.getModel();
		try {
			ResultSet rst = product.getProducts();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			int colCount = rsmd.getColumnCount();
			
			for (int i=1; i <= colCount; i++){	
				model.addColumn(rsmd.getColumnName(i).toString());
			}
			
			while(rst.next()) {
				model.addRow(new Object[] {rst.getString("ID").toString(),
						rst.getString("PRODUCT NAME").toString(),
						rst.getString("PRICE").toString(),
						rst.getString("STOCK").toString()});
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//back button
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
				MainWindow.newMainWindow();
			}
		});
		
		

		//save button
		saveBtn = new JButton("Save");
		saveBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(id != -1) {
					product.updatePrice(id, price);
					product.updateStock(id, stock);
					id = -1;
					pName = null;
					price = -1.f;
					stock = -1;
				}
			}
		});
		
		
		//delete button
		deleteBtn = new JButton("Delete");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(id != -1) {
					product.deleteProduct(id);
					id = -1;
					reloadTable();
				}
			}
		});
		
		
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				getTableRow(e);
			}
		});
		
		/*table.getModel().addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {	
				int row = table.getSelectedRow();
				id1 = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
		  		pName = table.getModel().getValueAt(row, 1).toString();
		  		price = Double.parseDouble(table.getModel().getValueAt(row, 2).toString());
		  		stock = Integer.parseInt(table.getModel().getValueAt(row, 3).toString());
			}
		});
		*/
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		contentPane2.add(backBtn);
		contentPane2.add(saveBtn);
		contentPane2.add(deleteBtn);
		
		contentPane.add(scrollPane);
		contentPane.add(contentPane2);
		setContentPane(contentPane);
	}
	
	
	private void reloadTable() {
		try {
			ResultSet rst = product.getProducts();
			ResultSetMetaData rsmd = (ResultSetMetaData) rst.getMetaData();
			model.setRowCount(0);
			int colCount = rsmd.getColumnCount();
			
			while(rst.next()) {
				model.addRow(new Object[] {rst.getString("ID").toString(),
						rst.getString("PRODUCT NAME").toString(),
						rst.getString("PRICE").toString(),
						rst.getString("STOCK").toString()});
			}
			model.fireTableDataChanged();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void getTableRow(MouseEvent e) {
		int row = table.getSelectedRow();
		id = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
		pName = table.getModel().getValueAt(row, 1).toString();
  		price = Double.parseDouble(table.getModel().getValueAt(row, 2).toString());
  		stock = Integer.parseInt(table.getModel().getValueAt(row, 3).toString());
	}
	
}
