package db;


import java.sql.*;
import java.io.*;
import java.lang.Integer;
import java.lang.Double;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

public class DBConnection {
	
	private static DBConnection databaseConnection;

	private Connection connection = null;
	private static Statement statement = null;
	private String password = null;
	
	public DBConnection() {
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehouse", "root", password);
			// Statements allow to issue SQL queries to the database
			statement = connection.createStatement();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

	
	public static DBConnection getInstance() {
		if (databaseConnection == null) {
			databaseConnection = new DBConnection();
		}
		return databaseConnection;
	}
	
	
	private void closeConnection()
	{	try {
			connection.close();
		}
		catch (SQLException ex) {
			System.err.println("Exception during connection close: " + ex);
		}
	}	
}