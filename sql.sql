DROP DATABASE IF EXISTS warehouse;
CREATE DATABASE IF NOT EXISTS warehouse;

USE warehouse;


CREATE TABLE Customer (
   CustomerId int unsigned not null auto_increment,
   firstName VARCHAR(30) NOT NULL,
   lastName VARCHAR(30) NOT NULL,
   PRIMARY KEY (CustomerId) 
);


CREATE TABLE Product (
   ProductId int unsigned not null auto_increment,
   ProductName varchar(50),
   ListPrice decimal (9,2),
   PRIMARY KEY (ProductId)
);


CREATE TABLE ProductQ (
	ProductId int unsigned not null unique,
    Stock int unsigned default 1,
    FOREIGN KEY (ProductId) REFERENCES Product (ProductId) ON DELETE CASCADE
);


CREATE TABLE Orders (
   OrderId int unsigned not null auto_increment,
   OrderDate datetime not null,
   CustomerId int unsigned not null,
   Total decimal (9,2) DEFAULT 0,
   PRIMARY KEY (OrderId),
   FOREIGN KEY (CustomerId) REFERENCES Customer (CustomerId) ON DELETE CASCADE
);


CREATE TABLE OrderDet (
	ProductId int unsigned not null,
    OrderId int unsigned not null,
    Quantity int unsigned not null,
	FOREIGN KEY (OrderId) REFERENCES Orders (OrderId) ON DELETE CASCADE,
	FOREIGN KEY (ProductId) REFERENCES Product (ProductId) ON DELETE CASCADE
);


DROP PROCEDURE IF EXISTS ADD_PRODUCT;
DELIMITER //
CREATE PROCEDURE ADD_PRODUCT(pName_ varchar(50), price_ decimal (9,2), stock_ int)
	BEGIN
		IF (SELECT Product.ProductName from Product where ProductName = pName_) IS NULL THEN
			INSERT INTO Product(ProductName, ListPrice) VALUES (pName_, price_);
        END IF;
        
        SET @idProduct = NULL;
        
        IF (SELECT @idProduct := Product.ProductId from Product where ProductName = pName_) IS NOT NULL THEN
			IF (SELECT ProductQ.ProductId from ProductQ where ProductQ.ProductId = @idProduct) IS NULL THEN
				INSERT INTO ProductQ(ProductId, Stock) VALUES (@idProduct, stock_);
            END IF;
        END IF;
        
    END //
DELIMITER ;


DROP PROCEDURE IF EXISTS ADD_ORDER;
DELIMITER //
CREATE PROCEDURE ADD_ORDER(fName varchar(30), lName varchar(30))
	BEGIN
		SET @CustId = NULL;
        IF (SELECT @CustId := Customer.CustomerId from Customer where Customer.firstName = fName and Customer.lastName = lName) IS NOT NULL THEN
			INSERT INTO Orders(OrderDate, CustomerId, Total) values (CURRENT_DATE(), @CustId, '0.00');
        END IF;
    END //
DELIMITER ;


DROP PROCEDURE IF EXISTS ADD_PRODUCT_TO_ORDER;
DELIMITER //
CREATE PROCEDURE ADD_PRODUCT_TO_ORDER(fName varchar(30), lName varchar(30), pName varchar(50), quantity int)
	BEGIN
		SET @custId = NULL;
        IF(SELECT @custId := Customer.CustomerId from Customer where Customer.firstName = fName and Customer.lastName = lName) is not null then
			SET @orderId = NULL;
			IF(SELECT @orderId := Orders.OrderId from Orders where Orders.CustomerId = @custId Order by Orders.OrderDate DESC LIMIT 1) IS NOT NULL THEN
				SET @prodId = NULL;
                SET @total = (SELECT Orders.Total from Orders where Orders.OrderId = @orderId);
				IF(SELECT @prodId := Product.ProductId from Product where Product.ProductName = pName) IS NOT NULL THEN
					IF(SELECT ProductQ.ProductId from ProductQ where ProductQ.ProductId = @prodId and ProductQ.Stock >= quantity) IS NOT NULL THEN
						INSERT INTO OrderDet(ProductId, OrderId, Quantity) values (@prodId, @orderId, quantity);
                        SET @price = (SELECT Product.ListPrice from Product where Product.ProductId = @prodId);
                        UPDATE Orders SET Orders.Total = @total + @price *quantity where Orders.OrderId = @orderId;
                        UPDATE ProductQ SET ProductQ.Stock = ProductQ.Stock - quantity where ProductQ.ProductId = @prodId;
					ELSE SELECT CONCAT('1');
					END IF;
				ELSE SELECT CONCAT('2');
				END IF;
			ELSE SELECT CONCAT('3');
			END IF;
            ELSE SELECT CONCAT('4');
		END IF;
    END //
DELIMITER ;


CREATE VIEW VIEW_PRODUCTS AS
	SELECT Product.ProductId AS 'ID', 
    Product.ProductName AS 'PRODUCT NAME', 
    Product.ListPrice AS 'PRICE', 
    ProductQ.Stock AS 'STOCK' 
    FROM (Product inner join ProductQ on Product.ProductId = ProductQ.ProductId);
    
    
CREATE VIEW VIEW_ORDERS AS
	SELECT Orders.OrderId AS 'ID',	
		Orders.OrderDate AS 'DATE',
        Customer.firstName AS 'FIRST NAME',
        Customer.lastName AS 'LAST NAME',
        Orders.Total AS 'TOTAL',
        Product.ProductName AS 'PRODUCT NAME',
        OrderDet.ProductId AS 'PRODUCT ID',
        OrderDet.Quantity AS 'QUANTITY'
        FROM (((Orders inner join Customer on Orders.CustomerId = Customer.CustomerId) 
        inner join OrderDet on Orders.OrderId = OrderDet.OrderId) 
        inner join Product on Product.ProductId = OrderDet.ProductId);